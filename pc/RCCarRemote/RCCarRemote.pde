import hypermedia.net.*;

import processing.serial.*;

UDP udp;

HashMap<Character, Boolean> keys = new HashMap<Character, Boolean>();
char[] controls = {'w', 's', 'a', 'd'};

boolean hasSentAll = false;

void setup() {
  udp = new UDP(this, 1234);
  
  for(int i = 0; i < controls.length; ++i){
    keys.put(controls[i], false);
  }
}

void send(char s) {
  println(s);
  udp.send(str(s), "192.168.43.77", 1235);
}

void draw(){
}

void keyPressed() {
  try{
    if(!keys.get(key)) send(key);
  }catch(NullPointerException ex) {
    
  }
  if (keys.get(key) != null) {
    keys.put(key, true);
  }
}

void keyReleased() {
  if (keys.get(key) != null) {
    keys.put(key, false);
  }
  if(!(keys.get('a') ^ keys.get('d'))) {
    send('g');
  }
  if(!(keys.get('w') ^ keys.get('s'))) {
    send('t');
  }
}
