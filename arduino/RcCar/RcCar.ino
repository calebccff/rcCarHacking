#include <Wire.h>

#define PIN_FD 9
#define PIN_RV 5

#define PIN_LF 10
#define PIN_RT 11

void setup() {
  // put your setup code here, to run once:
  pinMode(PIN_FD, OUTPUT);
  pinMode(PIN_RV, OUTPUT);
  pinMode(PIN_LF, OUTPUT);
  pinMode(PIN_RT, OUTPUT);

  // begin running as an I2C slave on the specified address
  Wire.begin(0x04);
  //Serial.begin(9600);
 
  // create event for receiving data
  Wire.onReceive(receiveData);
}

byte FD_SPEED = 0;
byte RV_SPEED = 0;
byte LF_SPEED = 0;
byte RT_SPEED = 0;
 
void loop() {
  // nothing needed here since we're doing event based code
  analogWrite(PIN_RV, min(RV_SPEED==0?0:RV_SPEED+100, 240));
  analogWrite(PIN_FD, min(FD_SPEED==0?0:FD_SPEED+100, 240));
  analogWrite(PIN_RT, min(RT_SPEED==0?0:RT_SPEED+180, 240));
  analogWrite(PIN_LF, min(LF_SPEED==0?0:LF_SPEED+180, 240));
}

/* BIT ENCODING
00 - FD
01 - RV
10 - LF
11 - RT
=== SPEEDS ===
000000 - STOP
000001 - MIN SPEED
111111 - MAX SPEED
 */
 
void receiveData(int byteCount) {
  if ( Wire.available()) {
    byte rec = Wire.read();
    byte motor = rec >> 6;
    byte speedBits = rec & 0b00111111;
    switch(motor) {
      case 0b00:
        FD_SPEED = speedBits;
        RV_SPEED = 0;
        break;
      case 0b01:
        RV_SPEED = speedBits;
        FD_SPEED = 0;
        break;
      case 0b10:
        LF_SPEED = speedBits;
        RT_SPEED = 0;
        break;
      case 0b11:
        RT_SPEED = speedBits;
        LF_SPEED = 0;
        break;
    }
  }
}

//char rx_byte = 0;
//
//void loop() {
//  // put your main code here, to run repeatedly:
//  if (Serial.available() > 0) {    // is a character available?
//    rx_byte = Serial.read();
//
//    switch(rx_byte) {
//      case 'w':
//        analogWrite(PIN_FW, 100);
//        analogWrite(PIN_RV, 0);
//        break;
//      case 's':
//        analogWrite(PIN_RV, 100);
//        analogWrite(PIN_FW, 0);
//        break;
//      case 'a':
//        analogWrite(PIN_LF, 255);
//        analogWrite(PIN_RT, 0);
//        break;
//      case 'd':
//        analogWrite(PIN_RT, 255);
//        analogWrite(PIN_LF, 0);
//        break;
//      case 't':
//        analogWrite(PIN_RV, 0);
//        analogWrite(PIN_FW, 0);
//        analogWrite(PIN_RT, 0);
//        analogWrite(PIN_LF, 0);
//        break;
//      case 'g':
//        analogWrite(PIN_RT, 0);
//        analogWrite(PIN_LF, 0);
//    }
//
//  }
//}
