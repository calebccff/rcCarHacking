import socket
import smbus

I2Cbus = smbus.SMBus(1) 

def sendData(data):
    print("Sending i2c data", data)
    try:
        I2Cbus.write_byte(0x04, data)
    except IOError:
        print("Failed to send")
 
localIP     = "0.0.0.0"
localPort   = 1235
bufferSize  = 256
 
# Create a datagram socket
UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
 
# Bind to address and ip
UDPServerSocket.bind((localIP, localPort))
 
print("UDP server up and listening")
 
# Listen for incoming datagrams
while(True):
    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
    message = bytesAddressPair[0]
    print(message)
    address = bytesAddressPair[1]
    if message == 'w':
        sendData(0b00001111)
    elif message == 's':
        sendData(0b01001111)
    elif message == 'a':
        sendData(0b10111011)
    elif message == 'd':
        sendData(0b11111011)
    elif message == 't':
        sendData(0b00000000)
        sendData(0b01000000)
    elif message == 'g':
        sendData(0b10000000)
        sendData(0b11000000)