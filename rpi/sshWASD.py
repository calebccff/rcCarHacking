import smbus
from getkey import getkey, keys

I2Cbus = smbus.SMBus(1) 

def sendData(data):
    I2Cbus.write_i2c_block_data(0x04, 0x00, [data]) 

def main():
    while 1:
        key = getkey()
        if key == 'w':
            sendData(0b00011111)
        elif key == 's':
            sendData(0b01011111)
        elif key == 'a':
            sendData(0b10111111)
        elif key == 'd':
            sendData(0b11111111)
        print(key)

# def on_key_release(key):
#     print(key, "UP")
#     #sendData('t')

# with keyboard.Listener(on_release = on_key_release) as listener:
#     listener.join()

main()