# RC Car Hacking!
This is a proof of concept project on reverse engineering and hacking a cheap RC car.

The car itself is a WLtoys 18428-B. It runs off a 4.8V lithium-ion battery. It has 3 motors, 2 of them are for driving and their signal pins are wired in parralel. The last is for turning the wheels.

## Step 1. Reverse engineering
The first step was figuring out how the car works, with just some simple probing it became fairly apparent that the large chip on top of the board was a motor driver, actually 2 motor drivers in one chip. The 2 inputs are used to specify which direction you want to car to travel in, there are 4 inputs total, the two forward inputs are wired together and so are the backwards inputs. Phew that was confusing...

The steering is a little more complicated, there is another motor driver (I think) chip on the back of the board, it has 2 inputs as well! One for turning left and one for turning right.

### Controlling the car
Luckily, it's as simple as pulling the input you want high, the higher you pull it the faster that motor will spin!
It appears that for turning, the wheels always take the same amount of time to turn before the clutch trips and the motor spins freely, this means the amount of power you send to the motor is directly proportional to how far the wheels will turn - this makes things extremely simple!

## Step 2. Proof of concept
The first thing to do was to hook up an arduino and get this thing rolling (Literally!!!). The first arduino code listened for serial input and checked if the character received was W, A, S or D. It then pulled the correct pin high to trigger movement in that direction. The code you see for the arduino now listens over I2C and received bytes with the following encoding:

```
BIT ENCODING
00 - FD
01 - RV
10 - LF
11 - RT
=== SPEEDS ===
000000 - STOP
000001 - MIN SPEED
111111 - MAX SPEED
```

## Step 3. Bake it
With an arduino that can drive the motors based on I2C commands, we're ready to make it wifi - this could theoretically all be done with an arduino however to make it more accessible and probably cheaper it made more sense to use a Pi Zero W.
The Raspberry Pi has some Python code (TODO) that boilerplates all communication with the arduino - the end result is anyone can write code in Python, import the module and call `forward(100)` to travel forward at 100% speed.

## Step 4. Your turn
We now have a fully hackable, wifi enabled IoT car, it's your turn to go and build interesting things with it, strap an ultrasonic sensor to it - or even a camera, GPS, the sky is the limit!